'use strict';

if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){
    window.location = "http://www.slash.co.il/projects/coca_cola/stella_special_edition/mobile/"
}
// if(!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)){
//     var myVideo = document.querySelector("#x-video");
//     $(".x-video-play").hide();
//     myVideo.setAttribute("controls","controls")
// }

var app = angular.module('app', ['ngRoute', 'ngAnimate', 'slickCarousel']);

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/one', {
            templateUrl: 'views/one.html'
        })
        .when('/two', {
            templateUrl: 'views/two.html'
        })
        .when('/three', {
            templateUrl: 'views/three.html'
        })
        .when('/three/bars', {
            templateUrl: 'views/three-tabs-bars.html'
        })
        .when('/three/restaurants', {
            templateUrl: 'views/three-tabs-restaurants.html'
        })
        .otherwise({
            redirectTo: '/one'
        });

    // $locationProvider.html5Mode(true);
}]);

app.controller('mainCtrl', ['$scope', '$location', '$timeout', function ($scope, $location, $timeout) {

    /*
     tabs
     */
    $scope.tab = 1;

    $scope.setTab = function (tab) {
        $scope.tab = tab;
    };

    $scope.isTab = function (tab) {
        return $scope.tab === tab;
    };


    /*
     modals
     */
    $scope.openModalN = 1; // on finish = 1

    $scope.openModal = function (openModalN) {
        $scope.openModalN = openModalN;
    };


    /*
     highlight a current menu item
     */
    $scope.curPage = function (path) {
        return $location.path().substr(0, (path ? path.length : 0)) === path;
    };






    $scope.ga = function (descormob, anevent) {
        ga('send', 'event', descormob, anevent, '');
    };
    
    $scope.pauseVideo = function (e) {
        if($(e.target).hasClass('slick-arrow')){
            $('iframe').each(function(){
                $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
            });
        }
    };
}]);



////////////////////////////////////////////

app.directive("toggleNext", function () {
    return {
        restrict: "A",
        link: function (scope, element, attr, form) {
            element.on("click", function () {
                $(element).next().toggleClass('active')
            });
        }
    };
});

// app.directive("playVideo", function () {
//     return {
//         restrict: "A",
//         link: function (scope, element, attr, form) {
//             element.on('click', function () {
//                 var thisVieo = $(this).next()[0];
//
//                 if (thisVieo.hasAttribute("controls")) {
//                     thisVieo.removeAttribute("controls")
//                 } else {
//                     thisVieo.setAttribute("controls","controls")
//                 }
//
//
//
//
//                 if (thisVieo.paused){
//                     element.hide();
//                     thisVieo.play();
//                 }
//                 else{
//                     element.show();
//                     thisVieo.pause();
//                 }
//             });
//         }
//     };
// });

app.directive("removeOnLoad", function ($timeout) {
    return {
        restrict: "A",
        link: function (scope, element, attr, form) {
            $timeout(function () {
                $('body').removeClass('loading');
                element.remove();
            }, 500);
        }
    };
});